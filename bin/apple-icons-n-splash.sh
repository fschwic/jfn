#!/bin/bash

CREATE_ALL=true
CREATE_ICONS=false
CREATE_SPLASHS=false
SHIFT=0

export MAGICK_HOME=/Users/fschwic/opt/ImageMagick-7.0.8
CONVERT=${MAGICK_HOME}/bin/convert
export DYLD_LIBRARY_PATH="${MAGICK_HOME}/lib/"

usage() {
    echo "$0 [OPTIONS] IMAGE_FILE"
    echo "  -i (only) create icons"
    echo "  -s (only) create splash images"
    echo "  -h show this help"
}

while getopts "ish" opt ; do
  case $opt in
    i)
      CREATE_ALL=false
      CREATE_ICONS=true
      SHIFT=$((SHIFT + 1))
      echo "i $SHIFT"
      ;;
    s)
      CREATE_ALL=false
      CREATE_SPLASHS=true
      SHIFT=$((SHIFT + 1))
      echo "s $SHIFT"
      ;;
    h)
      usage
      exit
      ;;
    \?)
      #echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

shift $SHIFT
if [ "x$1" == "x" ] ; then
    usage
    exit
fi
echo $1

# Icons
if $CREATE_ALL || $CREATE_ICONS ; then
    mkdir icons
    for s in 512 256 180 167 152 144 128 120 114 100 87 80 76 75 72 66 64 60 58 57 50 44 40 32 29 25 22 16 ; do
        #$CONVERT -size ${s}x${s} xc:none -scale ${s}x${s} $1 icons/apple-icon-${s}x${s}.png
        $CONVERT -size ${s}x${s} xc:none \( $1 -resize ${s}x${s} \) -gravity center -composite icons/apple-touch-icon-${s}x${s}.png
    done
fi

# Splash
if $CREATE_ALL || $CREATE_SPLASHS ; then
    mkdir img
    for g in 2732x2048 2048x2732 2208x1242 1242x2208 2048x1536 1536x2048 1334x750 1125x2436 2436x1125 \
            750x1334 1136x640 640x1136 1024x768 768x1024 960x640 640x960 320x568 568x320 320x480; do
        $CONVERT -size $g xc:black \( $1 -resize 620x620 \) -gravity center -composite img/apple-splash-${g}.png
    done
fi
