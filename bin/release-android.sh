#!/bin/bash

KEYSTOREFILE=$1
KEYALIAS=$2
# e.g. jfnkey or 5MW
BUILDNUMBER=$3

BIN_JARSIGNER=/usr/bin/jarsigner
BIN_ZIPALIGN=/Users/fschwic/Library/Android/sdk/build-tools/28.0.3/zipalign

# keytool -genkey -v -keystore my-release-key.keystore \
# -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

usage(){
    echo $0 KEYSTOREFILE KEYALIAS BUILDNUMBER
}

if [ "x$BUILDNUMBER" == "x" -o "x$KEYSTOREFILE" == "x" -o "x$KEYSTOREFILE" == "x-h" ]; then
    usage
    exit
fi

cordova build android --release
cd platforms/android/app/build/outputs/apk/release
$BIN_JARSIGNER -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTOREFILE app-release-unsigned.apk $KEYALIAS
mv app-release-unsigned.apk app-release-signed.apk
$BIN_ZIPALIGN -v 4 app-release-signed.apk app-release-aligned-${BUILDNUMBER}.apk
