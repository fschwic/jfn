var fs = require('fs');
var schedule = JSON.parse(fs.readFileSync('2019.json', 'utf8'));

function randomUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

function icsString(s) {
  var result;
  result = s.replace(/,/g, "\,");
  result = result.replace(/(\r\n|\n|\r)/gm, "\\n");
  //resutl = result.replace(/\n/g, "\\\n");
  return result;
}

function icsDate(s) {
  var result;
  result = s.replace(/\-/g, "");
  result = result.replace(/\:/g, "");
  return result;
}

function getDescription(t) {
  var result = "";
  result += t.description;
  result += "\\n\\n";
  t.speakers.forEach(function(speakerId) {
    schedule.speakers.forEach(function(s) {
      if (s.id === speakerId) {
        result += s.fullName + " \\n(" + s.tagLine + ")\\n\\n![Portait](" + s.profilePicture + " \"" + s.fullName + "\")\\n" + s.bio + "\\n\\n";
      }
    });
  });
  return result;
}

function getSpeaker(id) {
  var speaker = undefined;
  schedule.speakers.forEach(function(s) {
    if (s.id === id) {
      speaker = s;
    }
  });
  return speaker;
}


function location(id) {
  if (id == 3964) {
    return "Glashalle";
  } else if (id == 3965) {
    return "Future Meeting Space A";
  } else if (id == 3966) {
    return "Future Meeting Space B";
  } else if (id == 3967) {
    return "Konferenzraum 1";
  } else if (id == 3968) {
    return "Konferenzraum 2";
  }
  return "";
}

//console.log(schedule.sessions[0]);

console.log("BEGIN:VCALENDAR");
console.log("PRODID:-//Mozilla.org/NONSGML Mozilla Calendar V1.1//EN");
console.log("VERSION:2.0");


schedule.sessions.forEach(function(talk) {
  console.log("BEGIN:VEVENT");
  console.log("UID:" + "jfn-2019-" + talk.id);
  console.log("CREATED:20190901T100000Z");
  console.log("LAST-MODIFIED:20190915T100000Z");
  console.log("DTSTAMP:20190915T100000Z");
  var speaker = getSpeaker(talk.speakers[0]);
  if( speaker ){
    console.log("SUMMARY:" + icsString(talk.title + " (" + speaker.fullName + ")"));
  }
  else {
    console.log("SUMMARY:" + icsString(talk.title));
  }
  console.log("DESCRIPTION:" + icsString(getDescription(talk)));
  console.log("LOCATION:" + location(talk.roomId));
  console.log("CATEGORIES:Talk");
  console.log("DTSTART:" + icsDate(talk.startsAt));
  console.log("DTEND:" + icsDate(talk.endsAt));
  console.log("END:VEVENT");
});


console.log("END:VCALENDAR");
